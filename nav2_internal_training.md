# How-to-use this docker guide

The purpose of this docker is to allow you to use Rviz2 and rqt_gui for a turtlebot3 running 
ROS2 foxy from a remote pc that does not have ROS 2 foxy installed in it.

## To start using this docker container, 
1. ssh into your turtlebot using the terminal of your remote pc
```
ssh ubuntu@**ip address of the turtlebot**
```
and key in the password which is either **rosi** or **turtlebot3**

2. Get the ros_domain_id of the turtlebot3 by typing in the ssh terminal 
```
echo $ROS_DOMAIN_ID
```
Take note of this number

3. Go to the **remote.bash** of this package, nav2_internal_training_docker, find the 
line `docker-compose run -e ROS_DOMAIN_ID=32 remote_pc $@` and change it to the same value 
to that of the ROS_DOMAIN_ID of your turtlebot3

FYI: This is to allow multiple people to use similar turtlebots on the same network. 
The domain number differentiate the turtlebots from one another.

## Running the bash scripts
To run cartographer:
Enter in the terminal of your remote pc:
` ./cartographer.bash`

To run teleoperation:
Enter in the terminal of your remote pc:
` ./teleop.bash`

To run rqt_gui:
Enter in the terminal of your remote pc:
` ./rqt_gui.bash`

## Update OpenCR
To update opencr, get the **opencr_update.tar.bz2** file:

1. scp `path_to_your_opencr_update.tar.bz2` `ubuntu@**ip address of the turtlebot**`
2. ssh into your turtlebot using the terminal of your remote pc
3. Enter `export $OPENCR_MODEL=burger`
4. Enter `export OPENCR_PORT=/dev/ttyACM0`
5. Type in the terminal tar -xvf ./opencr_update.tar.bz2
6. cd ~/opencr_update/
7. To update the opencr, type in `/update.sh $OPENCR_PORT $OPENCR_MODEL.opencr`
8. Verify by using `./opencr_ld_shell_arm view burger.opencr`
